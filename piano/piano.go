package piano

import (
	"strings"
	"crypto/aes"
	"encoding/base64"
	"log"
	"crypto/sha256"
	"crypto/hmac"
)

const (
	DELIM        = "~~~"
	PADDING_CHAR = "X"
)

func Encrypt(keyString, value string) (token string, err error) {
	origKey := keyString

	if len(keyString) > 32 {
		keyString = keyString[:32]
	}

	if len(keyString) < 32 {
		keyString = rightPad2Len(keyString, "X", 32)
	}

	// Encrypt

	data, err := encryptAESCFB([]byte(value), []byte(keyString))
	if err != nil {
		return
	}

	safe := urlSafe(data)

	mac := hmac.New(sha256.New, []byte(origKey))
	mac.Write([]byte(safe))
	mac256 := mac.Sum(nil)

	return safe + DELIM + string(urlSafe(mac256)), nil
}

func Decrypt(keyString, payload string) (decrypetd string, err error) {

	data, err := urlDeSafe(strings.Split(payload, DELIM)[0])

	if err != nil {
		return
	}

	if len(keyString) > 32 {
		keyString = keyString[:32]
	}

	if len(keyString) < 32 {
		keyString = rightPad2Len(keyString, PADDING_CHAR, 32)
	}

	decoded, err := decryptAESCFB(data, []byte(keyString))
	if err != nil {
		return
	}

	return string(decoded), nil
}

func urlSafe(data []byte) string {
	return base64.RawURLEncoding.EncodeToString(data)
}

func urlDeSafe(data string) (bytes []byte, err error) {
	return base64.RawURLEncoding.DecodeString(data)
}

func rightPad2Len(s string, padStr string, overallLen int) string {
	var padCountInt int
	padCountInt = 1 + ((overallLen - len(padStr)) / len(padStr))
	var retStr = s + strings.Repeat(padStr, padCountInt)
	return retStr[:overallLen]
}

func encryptAESCFB(src, key []byte) ([]byte, error) {

	b, err := aes.NewCipher(key)

	if err != nil {
		log.Println(err)
	}

	padding := aes.BlockSize
	if diff := len(src) % padding; diff != 0 {
		padding = padding - diff
	}
	for i := 0; i < padding; i++ {
		src = append(src, byte(padding))
	}
	dst := make([]byte, len(src))

	blockSize := b.BlockSize()
	var ret []byte
	for len(src) > 0 {
		if len(src) >= blockSize {
			b.Encrypt(dst[:blockSize], src[:blockSize])
			ret = append(ret, dst[:blockSize]...)
			src = src[blockSize:]
			dst = dst[blockSize:]
		}
	}
	return ret, nil
}

func decryptAESCFB(src, key []byte) ([]byte, error) {

	b, err := aes.NewCipher(key)

	if err != nil {
		log.Println(err)
	}

	dst := make([]byte, len(src))

	blockSize := b.BlockSize()

	var rtn []byte
	for len(src) > 0 {
		if len(src) >= blockSize {
			b.Decrypt(dst[:blockSize], src[:blockSize])
			rtn = append(rtn, dst[:blockSize]...)
			src = src[blockSize:]
			dst = dst[blockSize:]
		}
	}

	padding := int(rtn[len(rtn)-1])
	rtn = rtn[:len(rtn)-padding]

	return rtn, nil
}
