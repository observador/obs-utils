package piano

import (
	"testing"
	"encoding/json"
)

const TEST_KEY = "abcdefghijkl"

func TestSum(t *testing.T) {

	pianoUser := struct {
		Uid       string    `json:"uid"`
		Email     string    `json:"email"`
		FirstName string    `json:"first_name"`
	}{
		"onePrettyID",
		"onePrettyEmail",
		"onePrettyFirstName",
	}

	serialized, err := json.Marshal(pianoUser)
	if err != nil {
		t.Errorf("error create serialize data %v", err)
	}

	r, err := Encrypt(TEST_KEY, string(serialized))
	if err != nil {
		t.Errorf("error create on Encrypt %v", err)
	}

	dec, err := Decrypt(TEST_KEY, r)
	if err != nil {
		t.Errorf("error create on Decrypt %v", err)
	}

	if dec != string(serialized) {
		t.Errorf("Encrypted and Decrypted but dont match")
	}
}
