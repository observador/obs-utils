package rabbit


type RabbitMessage struct {
	Queue   string
	Message []byte
}
