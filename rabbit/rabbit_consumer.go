package rabbit

import (
	"github.com/streadway/amqp"
	"log"
	"sync"
	"time"
)

type RabbitConsumer struct {
	uri             string
	queue           string
	connection      *amqp.Connection
	channel         *amqp.Channel
	wg              sync.WaitGroup
	ReceiverChannel chan amqp.Delivery
}

func NewRabbitConsumer(uri, queue string) (ra *RabbitConsumer, err error) {
	ra = new(RabbitConsumer)
	ra.queue = queue
	ra.uri = uri
	ra.ReceiverChannel = make(chan amqp.Delivery)
	err = ra.connect(false)
	if err != nil {
		ra.retryReconnect(err)
	}

	go func() {
		for {
			ra.retryReconnect(<-ra.connection.NotifyClose(make(chan *amqp.Error)))
		}
	}()
	return ra, nil
}

func (raC *RabbitConsumer) retryReconnect(err error) {
	ticker := time.NewTicker(time.Second * 5)
	for range ticker.C {
		log.Print("try reconnect...")
		if err := raC.connect(true); err != nil {
			log.Println("i can't reconnect", err)
			continue
		}
		ticker.Stop()
		break
	}
}

func (raC *RabbitConsumer) connect(reconnect bool) (err error) {
	if reconnect {
		if raC.channel != nil {
			raC.channel.Close()
		}
		if raC.connection != nil {
			raC.connection.Close()
		}
	}
	raC.connection, err = amqp.Dial(raC.uri)
	if err != nil {
		log.Println("Dial:", err)
		return err
	}

	log.Printf("got Connection, getting Channel")

	raC.channel, err = raC.connection.Channel()
	if err != nil {
		log.Fatalf("Dial: %s", err)
		log.Fatalf("Channel: %s", err)
	}

	deliveries, err := raC.channel.Consume(
		raC.queue, // name
		"",        // consumer
		false,     // auto-ack
		false,     // exclusive
		false,     // no-local
		false,     // no-wait
		nil,       // args
	)
	if err != nil {
		log.Printf("Queue Consume: %s", err)
		return err
	}

	go raC.handle(deliveries)
	return nil
}

func (raC *RabbitConsumer) handle(deliveries <-chan amqp.Delivery) {
	for d := range deliveries {
		raC.ReceiverChannel <- d
	}
}
