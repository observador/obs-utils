package rabbit

import (
	"log"
	"time"

	"github.com/streadway/amqp"
)

type RabbitProducer struct {
	uri          string
	topics       []string
	connection   *amqp.Connection
	channel      *amqp.Channel
	queues       map[string]*amqp.Queue
	SenderChanel chan RabbitMessage
}

func NewRabbitProducer(uri string, topics []string) (ra *RabbitProducer, err error) {

	senderChannel := make(chan RabbitMessage, 1)
	ra = new(RabbitProducer)
	ra.uri = uri
	ra.SenderChanel = senderChannel
	ra.topics = topics

	err = ra.connect(false)
	if err != nil {
		return ra, err
	}

	go func() {
		for {
			ra.onClose(<-ra.connection.NotifyClose(make(chan *amqp.Error)))
		}
	}()

	go ra.postMessage(senderChannel)

	return ra, err
}

func (ra *RabbitProducer) onClose(amqpErr *amqp.Error) {
	ticker := time.NewTicker(time.Second * 5)
	for range ticker.C {
		log.Print("try reconnect...")
		if err := ra.connect(true); err != nil {
			log.Printf("reconnect %v", err)
			continue
		}
		ticker.Stop()
		break
	}
}

func (ra *RabbitProducer) connect(reconnect bool) (err error) {
	if reconnect {
		if ra.channel != nil {
			ra.channel.Close()
		}
		if ra.connection != nil {
			ra.connection.Close()
		}
	}

	log.Printf("dialing %s\n", ra.uri)
	ra.connection, err = amqp.Dial(ra.uri)
	if err != nil {
		log.Printf("error dialing: %v\n", err)
		return err
	}

	log.Printf("Got Connection, getting Channel\n")
	ra.channel, err = ra.connection.Channel()
	if err != nil {
		log.Fatalf("erro connecting channel: %s\n", err)
		return err
	}

	err = ra.createQueues()
	if err != nil {
		return err
	}

	return nil
}

func (ra *RabbitProducer) disconnect() {
	defer ra.connection.Close()
}

func (ra *RabbitProducer) createQueues() (err error) {

	queues := make(map[string]*amqp.Queue)
	for _, t := range ra.topics {
		log.Print(t)
		q, err := ra.channel.QueueDeclare(
			t,     // name
			true,  // durable
			false, // delete when unused
			false, // exclusive
			false, // no-wait
			nil,   // arguments
		)
		if err != nil {
			return err
		}
		queues[t] = &q
	}
	ra.queues = queues
	return nil
}

func (ra *RabbitProducer) postMessage(mm <-chan RabbitMessage) {
	for {
		n, ok := <-mm
		if ok {
			err := ra.channel.Publish(
				"",      // exchange
				n.Queue, // routing key
				false,   // mandatory
				false,   // immediate
				amqp.Publishing{
					DeliveryMode: amqp.Persistent,
					ContentType:  "application/json",
					Body:         n.Message,
				},
			)
			if err != nil {
				log.Printf("Error send message: %v", err)
			}
		}
	}
}
