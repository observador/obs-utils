package barbeiroutils

import (
	"time"

	"bitbucket.org/observador/obs-utils/v2/peopleutils"
)

type CommentMessage struct {
	Type    string `json:"type"`
	Content struct {
		Id          int              `json:"id"`
		Text        string           `json:"text"`
		Status      string           `json:"status"`
		ContentType string           `json:"content_type"`
		ParentID    *int             `json:"parent_id"`
		ObjectID    string           `json:"object_id"`
		CreatedAt   time.Time        `json:"created_at"`
		Deleted     bool             `json:"deleted"`
		CreatedBy   peopleutils.User `json:"created_by"`
	} `json:"content"`
	ReplyTo *struct {
		Id          int              `json:"id"`
		Text        string           `json:"text"`
		Status      string           `json:"status"`
		ContentType string           `json:"content_type"`
		ParentID    *int             `json:"parent_id"`
		ObjectID    string           `json:"object_id"`
		CreatedAt   time.Time        `json:"created_at"`
		Deleted     bool             `json:"deleted"`
		CreatedBy   peopleutils.User `json:"created_by"`
	} `json:"reply_to"`
	Reporters []string `json:"reporters"`
	Value     float64  `json:"value"`
}
