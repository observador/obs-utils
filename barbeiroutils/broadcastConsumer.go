package barbeiroutils

import (
	"log"

	"github.com/cenkalti/backoff"
	"github.com/streadway/amqp"
)

type CallbackBroadcasterFunc func(msg []byte)

type BroadcastConsumer struct {
	uri          string
	exchangeName string
	conn         *amqp.Connection
	ch           *amqp.Channel
	queue        *amqp.Queue
	isConnected  bool
	callback     CallbackBroadcasterFunc
	RChannel     chan []byte
}

func NewBroadcastConsumer(uri, exchangeName string, fn CallbackBroadcasterFunc) (brodCons *BroadcastConsumer) {
	brodCons = &BroadcastConsumer{}

	brodCons.uri = uri
	brodCons.exchangeName = exchangeName
	brodCons.RChannel = make(chan []byte, 1)
	brodCons.callback = fn

	err := brodCons.connect(false)
	if err != nil {
		log.Println(err)
		return nil
	}
	//handle with errors
	go func() {
		for {
			er := <-brodCons.conn.NotifyClose(make(chan *amqp.Error))
			brodCons.retryReconnect(er)
		}
	}()
	return brodCons
}

func (brodCons *BroadcastConsumer) connect(reconnect bool) (err error) {
	done := make(chan error)
	if reconnect {
		if brodCons.ch != nil {
			brodCons.ch.Close()
		}
		if brodCons.conn != nil {
			brodCons.conn.Close()
		}
	}
	brodCons.conn, err = amqp.Dial(brodCons.uri)
	if err != nil {
		log.Println("Failed to connect to RabbitMQ:", err)
		return err
	}
	// defer conn.Close()
	brodCons.ch, err = brodCons.conn.Channel()
	if err != nil {
		log.Println("Failed to open a channel:", err)
		return err
	}

	// defer ch.Close()
	err = brodCons.ch.ExchangeDeclare(
		brodCons.exchangeName, // name
		"fanout",              // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		log.Println("Failed to declare an exchange: %s", err)
		return err
	}

	q, err := brodCons.ch.QueueDeclare(
		"",    // name
		false, // durable
		true,  // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		log.Println("Failed to declare a queue:", err)
		return err
	}
	brodCons.queue = &q

	err = brodCons.ch.QueueBind(
		brodCons.queue.Name, // queue name
		"",                  // routing key
		brodCons.exchangeName, // exchange
		false,
		nil)
	if err != nil {
		log.Println("Failed to bind a queue: %s", err)
		return err
	}

	deliveries, err := brodCons.ch.Consume(
		brodCons.queue.Name, // queue
		"",                  // consumer
		true,                // auto-ack
		false,               // exclusive
		false,               // no-local
		false,               // no-wait
		nil,                 // args
	)
	if err != nil {
		log.Println("Failed to register a consumer:", err)
		return err
	}
	log.Println("consumer started with success")
	go brodCons.handle(deliveries, done)
	return
}

//TODO: check date
func (brodCons *BroadcastConsumer) handle(deliveries <-chan amqp.Delivery, done chan error) {
	for d := range deliveries {
		brodCons.callback(d.Body)
	}

	log.Printf("handle: deliveries channel closed")
	done <- nil
}

func (brodCons *BroadcastConsumer) retryReconnect(amqpErr error) {
	log.Println("connection lost", amqpErr)
	brodCons.isConnected = false

	b := backoff.NewExponentialBackOff()
	ticker := backoff.NewTicker(b)
	for range ticker.C {
		log.Print("try reconnect...")
		if err := brodCons.connect(true); err != nil {
			log.Println("i can't reconnect", err)
			continue
		}
		ticker.Stop()
		break
	}
}
