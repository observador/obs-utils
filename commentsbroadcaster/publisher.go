package commentsbroadcaster

import (
	"fmt"
	"log"

	"github.com/cenkalti/backoff"
	"github.com/streadway/amqp"
)

type Publisher struct {
	uri          string
	exchangeName string
	conn         *amqp.Connection
	ch           *amqp.Channel
	isConnected  bool
}

func NewPublisher(uri, exchangeName string) (pub *Publisher) {
	pub = &Publisher{}
	pub.uri = uri
	pub.exchangeName = exchangeName

	pub.connect()

	//handle with errors
	go func() {
		for {
			pub.retryReconnect(<-pub.conn.NotifyClose(make(chan *amqp.Error)))
		}
	}()

	return pub
}

func (pub *Publisher) reconnect() (err error) {
	//before reconnect close all connections
	pub.ch.Close()
	pub.conn.Close()

	log.Println("reconnecting %q", pub.uri)
	connection, err := amqp.Dial(pub.uri)
	if err != nil {
		return
	}
	pub.conn = connection

	log.Println("got Connection, getting Channel")
	pub.ch, err = connection.Channel()
	pub.isConnected = true

	return
}

func (pub *Publisher) connect() {
	var err error
	pub.conn, err = amqp.Dial(pub.uri)
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %s", err)
		panic(fmt.Sprintf("Failed to connect to RabbitMQ: %s", err))
	}
	// defer pub.conn.Close()

	pub.ch, err = pub.conn.Channel()
	if err != nil {
		log.Fatalf("Failed  to open a channel: %s", err)
		panic(fmt.Sprintf("Failed  to open a channel: %s", err))
	}
	// defer pub.ch.Close()

	err = pub.ch.ExchangeDeclare(
		pub.exchangeName, // name
		"fanout",            // type
		true,                // durable
		false,               // auto-deleted
		false,               // internal
		false,               // no-wait
		nil,                 // arguments
	)
	if err != nil {
		log.Fatalf("Failed to declare an exchange: %s", err)
		panic(fmt.Sprintf("Failed to declare an exchange: %s", err))
	}
}

func (pub *Publisher) retryReconnect(amqpErr *amqp.Error) {
	log.Println("connection lost", amqpErr.Error())
	pub.isConnected = false

	b := backoff.NewExponentialBackOff()
	ticker := backoff.NewTicker(b)
	for range ticker.C {
		log.Print("try reconnect...")
		if err := pub.reconnect(); err != nil {
			log.Println("i can't reconnect", err)
			continue
		}
		ticker.Stop()
		break
	}
}

//SendMessage Send fucking message
func (pub *Publisher) SendMessage(msg []byte) (err error) {
	err = pub.ch.Publish(
		pub.exchangeName, // exchange
		"",                  // routing key
		false,               // mandatory
		false,               // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        msg,
		},
	)
	return err
}