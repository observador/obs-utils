package commentsbroadcaster

import "time"

type CommentMessage struct {
	Type    string `json:"type"`
	Content struct {
		Text        string    `json:"text"`
		Status      string    `json:"status"`
		ContentType string    `json:"content_type"`
		ObjectID    string    `json:"object_id"`
		CreatedAt   time.Time `json:"created_at"`
		Deleted     bool      `json:"deleted"`
	} `json:"content"`
	Value float64 `json:"value"`
}
