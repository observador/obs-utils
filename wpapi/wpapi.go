package wpapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/observador/obs-utils/v2/utils"
	"github.com/ReneKroon/ttlcache"
	"github.com/cenkalti/backoff"
)

type PostResponse struct {
	ID        int       `json:"id"`
	Title     string    `json:"title"`
	FullTitle string    `json:"fullTitle"`
	Lead      string    `json:"lead"`
	PubDate   time.Time `json:"pubDate"`
	Links     struct {
		WebURI string `json:"webUri"`
		URI    string `json:"uri"`
	} `json:"links"`
	Type     string `json:"type"`
	Image    string `json:"image"`
	Metadata struct {
		Topics []struct {
			ID    int    `json:"id"`
			Name  string `json:"name"`
			Style struct {
				BackgroundColor string `json:"backgroundColor"`
				ForegroundColor string `json:"foregroundColor"`
			} `json:"style,omitempty"`
			AssociatedURL string `json:"associatedUrl"`
			MainTopic     bool   `json:"mainTopic"`
		} `json:"topics"`
		Authors []struct {
			ID          int      `json:"id"`
			Name        string   `json:"name"`
			URI         string   `json:"uri"`
			Image       string   `json:"image"`
			Description string   `json:"description"`
			Role        string   `json:"role"`
			CreditType  []string `json:"creditType"`
			Location    string   `json:"location"`
			FreeText    string   `json:"freeText"`
		} `json:"authors"`
		MainVideo bool `json:"main_video"`
		Gallery   struct {
			PhotosCount int `json:"photosCount"`
		} `json:"gallery"`
		Program struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
			Slug  string `json:"slug"`
		} `json:"program"`
		Duration string `json:"duration"`
		Headline bool   `json:"headline"`
		Featured bool   `json:"featured"`
	} `json:"metadata"`
	State         string `json:"state"`
	SquareImage   string `json:"square_image"`
	GalleryImages []struct {
		Caption string `json:"caption"`
		Credit  string `json:"credit"`
		URI     string `json:"uri"`
		Height  int    `json:"height"`
		Width   int    `json:"width"`
	} `json:"galleryImages"`
	Body            string `json:"body"`
	Premium         bool   `json:"premium"`
	CommentsEnabled bool   `json:"comments_enabled"`
}

type PostTopic struct {
	ID   int64
	Name string
}

type PostAuthor struct {
	ID    int64
	Name  string
	Image string
}

type PostProgram struct {
	ID    int64
	Title string
}
type PostStruct struct {
	Id          int64        `json:"id"`
	Title       string       `json:"title"`
	Image       string       `json:"image"`
	WebUri      string       `json:"webUri"`
	Type        string       `json:"type"`
	Fotogallery int          `json:"fotoGallery"`
	Topics      []PostTopic  `json:"topics"`
	Authors     []PostAuthor `json:"authors"`
	Program     PostProgram  `json:"program"`
}

var cache *ttlcache.Cache
var apiUrl string

func init() {
	cache = ttlcache.NewCache()
	cache.SetTTL(time.Duration(30 * time.Second))
	apiUrl = "https://api.observador.pt/wp/"
}

// GetPostInfo call api observador v3 to get post info
func GetPostInfo(postID int64) (p PostStruct, cached bool, err error) {
	strPostID := strconv.Itoa(int(postID))

	cachedValue, exists := cache.Get(strPostID)
	if exists {
		p = cachedValue.(PostStruct)
		return p, true, err
	}

	/**
	 * retry and backoff to get post data..
	 * como o wordpress sucks, quando envia a notificação os dados ainda nao estão guardados,
	 * então usamos um algoritmos de retry and backoff para repetir o pedido até termos os dados..
	 */

	firstRun := true
	nt := 0
	//retry 5 seconds after
	b := backoff.NewExponentialBackOff()
	b.InitialInterval = 5 * time.Second
	ticker := backoff.NewTicker(b)

	for range ticker.C {
		if firstRun {
			// in first run dont do anything
			firstRun = false
			continue
		}
		//get post info
		p, err = restGetPostInfo(postID)
		nt++
		if err != nil && err.Error() == "nodata" {
			ticker.Stop()
			break
		}
		// se o post nao for valido volta a tentar!
		if p.Type == "" || (p.Type != "opinion" && p.Image == "") || p.Title == "" || len(p.Authors) == 0 {
			if nt < 3 {
				continue
			}
			break
		}

		ticker.Stop()
		break
	}
	//se passar mais que 15min, return error
	if p.Type == "" || (p.Type != "opinion" && p.Image == "") || p.Title == "" || len(p.Authors) == 0 {
		return p, false, errors.New("i can't get contents")
	}

	//set cache for other worker!
	cache.Set(strPostID, p)

	return p, false, err
}

func restGetPostInfo(postID int64) (p PostStruct, err error) {
	url := fmt.Sprintf("%sitems/id/%d", apiUrl, postID)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
	}

	req.Header.Set("User-Agent", fmt.Sprintf("OBS%s", utils.GetUserAgent()))

	httpClient := &http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return p, err
	}

	defer resp.Body.Close()
	if resp.StatusCode <= 400 {
		defer resp.Body.Close()
		postBody, err := ioutil.ReadAll(resp.Body)

		if err == nil {
			var postResponse PostResponse

			err := json.Unmarshal(postBody, &postResponse)
			if err != nil {
				log.Println(err)
				return p, err
			}
			var post PostStruct
			post.Id = postID
			post.Title = postResponse.Title
			post.WebUri = postResponse.Links.WebURI
			post.Type = postResponse.Type
			post.Image = postResponse.Image
			if postResponse.GalleryImages != nil {
				post.Fotogallery = 1
			} else {
				post.Fotogallery = 0
			}

			if postResponse.Metadata.Topics != nil {
				for _, top := range postResponse.Metadata.Topics {
					var tmpTopic PostTopic
					tmpTopic.ID = int64(top.ID)
					tmpTopic.Name = top.Name
					post.Topics = append(post.Topics, tmpTopic)
				}

			}

			if postResponse.Metadata.Authors != nil {
				for _, aut := range postResponse.Metadata.Authors {
					if aut.CreditType[0] != "free_text" {
						var tmpAuthor PostAuthor
						tmpAuthor.ID = int64(aut.ID)
						tmpAuthor.Name = aut.Name

						tmpAuthor.Image = aut.Image

						post.Authors = append(post.Authors, tmpAuthor)
					}
				}

			}
			post.Program.ID = int64(postResponse.Metadata.Program.ID)
			post.Program.Title = postResponse.Metadata.Program.Title

			return post, nil
		}

	}

	return p, fmt.Errorf(resp.Status)
}
