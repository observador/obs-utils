package jsonrestmiddleware

import (
	"crypto/rsa"
	"encoding/csv"
	"strings"

	"bitbucket.org/observador/obs-utils/v2/model"
	"github.com/ant0ine/go-json-rest/rest"
	"github.com/dgrijalva/jwt-go"

	"fmt"
)

type JwtDecoder struct {
	VerifyKey *rsa.PublicKey
}

func (jDecoder *JwtDecoder) MiddlewareFunc(handler rest.HandlerFunc) rest.HandlerFunc {
	return func(writer rest.ResponseWriter, request *rest.Request) {
		var user model.User
		request.Env["user"] = &user

		tokenSplit := strings.Split(request.Header.Get("Authorization"), "Bearer ")
		if len(tokenSplit) < 2 {
			handler(writer, request)
			return
		}

		tokenDecoded, err := jwt.Parse(tokenSplit[1], func(token *jwt.Token) (interface{}, error) {
			// Don't forget to validate the alg is what you expect:
			if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			return jDecoder.VerifyKey, nil
		})
		if err != nil {
			handler(writer, request)
			return
		}

		claims, ok := tokenDecoded.Claims.(jwt.MapClaims)
		if !ok && !tokenDecoded.Valid {
			handler(writer, request)
			return
		}

		user.Verified = true

		user.Uid, _ = claims["sub"].(string)
		user.Name, _ = claims["name"].(string)
		user.Staff, _ = claims["staff"].(bool)
		user.Risk, _ = claims["risk_level"].(int)
		user.Email, _ = claims["email"].(string)
		user.FamilyName, _ = claims["family_name"].(string)
		user.GivenName, _ = claims["given_name"].(string)
		user.Name, _ = claims["name"].(string)
		user.Nickname, _ = claims["nickname"].(string)
		user.Picture, _ = claims["picture"].(string)
		user.Staff, _ = claims["staff"].(bool)
		tags, _ := claims["tags"].(string)

		t := csv.NewReader(strings.NewReader(tags))
		rtags, _ := t.Read()
		for _, value := range rtags {
			user.Tags = append(user.Tags, model.Tag{Name: value})
		}

		model.ParseTags(&user)

		handler(writer, request)
		return
	}
}
