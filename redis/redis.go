package redis

import (
	"github.com/go-redis/redis"
	"time"
	"log"
	"encoding/json"
)

type RedisClient struct {
	redis *redis.Client
}

func NewRedisClient(options *redis.Options) (*RedisClient, error) {
	redisInstance := redis.NewClient(options)

	_, err := redisInstance.Ping().Result()
	if err != nil {
		return &RedisClient{}, err
	}

	client := &RedisClient{
		redis: redisInstance,
	}

	return client, nil
}

// Get save object in redis
func (rc *RedisClient) Set(key string, object interface{}, t time.Duration) error {
	b, err := json.Marshal(object)
	if err != nil {
		log.Printf("Error marshal to redis: %v\n", err)
		return err
	}
	err = rc.redis.Set(key, b, t).Err()
	return err
}

// Get get one key from redis
func (rc *RedisClient) Get(key string, v interface{}) error {
	cachedStr, err := rc.redis.Get(key).Result()
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(cachedStr), v)
	return err
}

// Del delete one key from redis
func (rc *RedisClient) Del(key string) error {
	_, err := rc.redis.Del(key).Result()
	return err
}

// DelWildcard delete from redis with wildcard
func (rc *RedisClient) DelWildcard(pattern string) (err error) {
	var keys []string

	rc.GetKeysForPattern(pattern, 0, 100, &keys)

	if err == nil {
		for _, key := range keys {
			_, err = rc.redis.Del(key).Result()
			log.Println("delete old key: ", key, err)
		}
	}

	return err
}

// GetKeysForPattern all keys with pattern
func (rc *RedisClient) GetKeysForPattern(pattern string, cursor uint64, size int64, keys *[]string) {
	tmpKeys, nextCursor, err := rc.redis.Scan(cursor, pattern, size).Result()
	if err == nil {
		*keys = append(*keys, tmpKeys...)
		if nextCursor > 0 {
			rc.GetKeysForPattern(pattern, nextCursor, size, keys)
		}
	}
	return
}

