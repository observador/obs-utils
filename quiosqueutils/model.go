// This package really shouldn't exist, it only makes life harder.
// Packages who depend on quiosque (its consumers) should depend on quiosque, not this.
// An argument could be made that packages that depend on quiosque should be in quiosque, but that's another story.
package quiosqueutils

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
)

type TokenProvider uint

const (
	TokenProviderOneSignal TokenProvider = iota
	TokenProviderFirebase
	TokenProviderOneSignalV4
)

func (tp TokenProvider) String() string {
	switch tp {
	case TokenProviderOneSignal:
		return "OneSignal"
	case TokenProviderFirebase:
		return "Firebase"
	case TokenProviderOneSignalV4:
		return "OneSignalV4"
	default:
		return "unknown"
	}
}

type Token struct {
	Value    string        `json:"value"`
	Provider TokenProvider `json:"provider"`
	Data     TokenData     `json:"data"`
}

func (t Token) OneSignalID() (string, bool) {
	if t.Provider != TokenProviderOneSignal && t.Provider != TokenProviderOneSignalV4 {
		return "", false
	}

	v, ok := t.Data["id"]
	if !ok {
		return "", false
	}

	vv, ok := v.(string)
	if !ok {
		return "", false
	}

	return vv, true
}

func (t Token) OneSignalDeviceType() (int, bool) {
	if t.Provider != TokenProviderOneSignal && t.Provider != TokenProviderOneSignalV4 {
		return -1, false
	}

	v, ok := t.Data["type"]
	if !ok {
		return -1, false
	}

	vv, ok := v.(int)
	if !ok {
		return -1, false
	}

	return vv, true
}

type TokenData map[string]interface{}

// this has no business being here, but it's a side-effect of the architecture. See package docs.
func (td TokenData) Value() (driver.Value, error) {
	// td cannot be a pointer receiver
	if td == nil {
		return nil, nil
	}

	if len(td) == 0 {
		return nil, nil
	}

	data, err := json.Marshal(td)
	if err != nil {
		return nil, err
	}
	return driver.Value(data), nil
}

// this has no business being here, but it's a side-effect of the architecture. See package docs.
func (td *TokenData) Scan(src interface{}) error {
	switch src := src.(type) {
	case []byte:
		var v map[string]interface{}
		if err := json.Unmarshal(src, &v); err != nil {
			return err
		}
		*td = TokenData(v)
		return nil
	case nil:
		return nil
	default:
		return errors.New("expected value to be a []byte")
	}
}

type QuisqueMsgDevice struct {
	// Deprecated: will only be present if ProviderType is ProviderOneSignal, use TokenV2 instead.
	PlayerId string `json:"player_id,omitempty"`
	// Deprecated: will only be present if ProviderType is ProviderOneSignal, use TokenV2 instead.
	Token   string `json:"token,omitempty"`
	TokenV2 Token  `json:"token_data,omitempty"`
}

type QuisqueMsgUsers struct {
	Id          string             `json:"id,omitempty"`
	Periodicity int                `json:"periodicity,omitempty"`
	Devices     []QuisqueMsgDevice `json:"device,omitempty"`
}
type QuisqueMsgMessage struct {
	Context           string            `json:"context,omitempty"`
	ContextInstanceId int               `json:"context_instance_id,omitempty"`
	Users             []QuisqueMsgUsers `json:"users,omitempty"`
}

type QuisqueMsgPush struct {
	PushText     string                  `json:"push_text"`
	PushType     string                  `json:"push_type"`
	PushImage    string                  `json:"push_image,omitempty"`
	DeviceCounts map[TokenProvider]int64 `json:"device_counts"`
}

type RabbitQuiosqueMessage struct {
	Id        int64               `json:"id"`
	PostID    int64               `json:"post_id"`
	PostSubID *int64              `json:"post_sub_id"`
	Messages  []QuisqueMsgMessage `json:"messages,omitempty"`
	Push      QuisqueMsgPush      `json:"push,omitempty"`
}
