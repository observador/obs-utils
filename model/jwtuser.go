package model

import "time"

type Group struct {
	Id         string    `json:"id,omitempty"`
	Name       string    `json:"name,omitempty"`
	AdminId    string    `json:"admin_id,omitempty"`
	AdminName  string    `json:"admin_name,omitempty"`
	AdminEmail string    `json:"admin_email,omitempty"`
	State      string    `json:"state,omitempty"`
	CreatedAt  time.Time `json:"created_at,omitempty"`
}

type User struct {
	Uid         string `json:"id"`
	Name        string `json:"name"`
	Nickname    string `json:"nickname"`
	GivenName   string `json:"given_name"`
	FamilyName  string `json:"family_name"`
	Picture     string `json:"picture"`
	Verified    bool   `json:"verified"`
	IsAnonymous bool   `json:"isAnonymous"`
	Email       string `json:"email"`
	Staff       bool   `json:"staff"`
	Group       *Group `json:"group"`
	Commenter   bool   `json:"commenter"`
	Risk        int    `json:"risk"`
	Tags        []Tag  `json:"tags"`
	IsPremium   bool   `json:"is_premium"`
}

func (u *User) IsUserAllowed(userID string) bool {
	return u.IsValid() && u.Uid != "" && u.Uid == userID
}
func (u *User) IsValid() bool {
	return u.Verified
}

type Tag struct {
	Name string `json:"name"`
}

func ParseTags(user *User) {
	for _, tag := range user.Tags {
		switch tag.Name {
		case "commenter":
			user.Commenter = true
		case "staff":
			user.Staff = true
		case "admin":
			user.Staff = true
		case "journalist":
			user.Staff = true
		case "moderator":
			user.Staff = true
		}
	}
}
