package redis_v2

import (
	"github.com/mediocregopher/radix.v2/pool"
	"github.com/mediocregopher/radix.v2/util"
	"encoding/json"
	"time"
)

type RedisPool struct {
	redisPool *pool.Pool
}

func NewRedisPool(host string) (*RedisPool, error) {
	p, err := pool.New("tcp", host, 10)
	return &RedisPool{
		redisPool: p,
	}, err

}

func (rp *RedisPool) Set(key string, object interface{}, t time.Duration) error {
	b, err := json.Marshal(object)
	if err != nil {
		return err
	}
	// get connection
	conn, err := rp.redisPool.Get()
	defer rp.redisPool.Put(conn)
	if err != nil {
		return err
	}

	err = conn.Cmd("SET", key, b).Err
	if err != nil {
		return err
	}

	exp := t.Seconds()

	err = conn.Cmd("EXPIRE", key, int(exp)).Err
	if err != nil {
		return err
	}

	return err
}

// Get get one key from redis
func (rp *RedisPool) Get(key string, v interface{}) error {
	// get connection
	conn, err := rp.redisPool.Get()
	defer rp.redisPool.Put(conn)
	if err != nil {
		return err
	}

	// execute command
	cachedStr, err := conn.Cmd("GET", key).Str()
	// release connection
	if err != nil {
		return err
	}

	err = json.Unmarshal([]byte(cachedStr), v)
	return err
}

// Del delete one key from redis
func (rp *RedisPool) Del(key string) error {
	// get connection
	conn, err := rp.redisPool.Get()
	defer rp.redisPool.Put(conn)
	if err != nil {
		return err
	}

	err = conn.Cmd("DEL", key).Err



	return err
}


//scan 0 MATCH *11*
// GetKeysForPattern all keys with pattern
func (rp *RedisPool) GetKeysForPattern(pattern string, size int, keys *[]string) error {
	conn, err :=  rp.redisPool.Get()
	defer rp.redisPool.Put(conn)

	if err != nil {
		return err
	}

	s := util.NewScanner(conn, util.ScanOpts{Command: "SCAN", Pattern: pattern, Count: size})
	for s.HasNext() {
		*keys = append(*keys, s.Next())
	}
	err = s.Err()
	return  err

}

// DelWildcard delete from redis with wildcard
func (rp *RedisPool) DelWildcard(pattern string) (err error) {
	var keys []string

	err = rp.GetKeysForPattern(pattern, 10, &keys)

	if err == nil {
		for _, key := range keys {
			err = rp.Del(key)
		}
	}

	return err
}
