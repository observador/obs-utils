package redis_v2

import (
	"testing"
	"time"
)

type User struct {
	Name string `json:"name"`
	Age  int16  `json:"age"`
}

const (
	DURATION = 10
)

func TestFirst(t *testing.T) {

	pessoa := &User{
		"Antonio",
		55,
	}

	rp, err := NewRedisPool("localhost:6379")
	if err != nil {
		t.Errorf("error create conncetion: %v", err)
	}

	err = rp.Set("test_user", pessoa, DURATION*time.Second)
	if err != nil {
		t.Errorf("error set value: %v", err)
	}

	novaPessoa := &User{}
	err = rp.Get("test_user", novaPessoa)
	if err != nil {
		t.Errorf("error get value: %v", err)
	}

	novaPessoa = &User{}
	timer := time.NewTimer(time.Second * (DURATION + 2))
	<-timer.C

	err = rp.Get("test_user", novaPessoa)
	if err == nil && novaPessoa.Name == "" {
		t.Errorf("value should be nil!")
	}

	pessoa2 := &User{
		"Antonio",
		55,
	}
	err = rp.Set("test_user", pessoa2, DURATION*time.Second)
	if err != nil {
		t.Errorf("error set value: %v", err)
	}

	err = rp.Del("test_user")
	if err != nil {
		t.Errorf("error set value: %v", err)
	}

}

func TestSecond(t *testing.T) {
	pessoa := &User{
		"Antonio",
		55,
	}

	rp, err := NewRedisPool("localhost:6379")
	if err != nil {
		t.Errorf("error create conncetion: %v", err)
	}

	err = rp.Set("test_user1", pessoa, DURATION*time.Second)
	if err != nil {
		t.Errorf("error set value: %v", err)
	}
	err = rp.Set("test_user2", pessoa, DURATION*time.Second)
	if err != nil {
		t.Errorf("error set value: %v", err)
	}

	err = rp.Set("test_user3", pessoa, DURATION*time.Second)
	if err != nil {
		t.Errorf("error set value: %v", err)
	}

	err = rp.Set("test_user4", pessoa, DURATION*time.Second)
	if err != nil {
		t.Errorf("error set value: %v", err)
	}

	err = rp.Set("test_user5", pessoa, DURATION*time.Second)
	if err != nil {
		t.Errorf("error set value: %v", err)
	}

	err = rp.Set("test_user7", pessoa, DURATION*time.Second)
	if err != nil {
		t.Errorf("error set value: %v", err)
	}

	err = rp.Set("test_user6", pessoa, DURATION*time.Second)
	if err != nil {
		t.Errorf("error set value: %v", err)
	}

	var a []string
	rp.GetKeysForPattern("test*", 10, &a)
	t.Log(a)

	rp.DelWildcard("test*")

	var b []string
	rp.GetKeysForPattern("test*", 10, &b)
	t.Log(b)
}
