package obsmiddleware

import "github.com/labstack/echo"

type FilterFunc func(echo.Context) bool

type AppKeyMiddleware struct {
	AuthKeys map[string]string
	Filter   FilterFunc
}

func (akm *AppKeyMiddleware) keyExist(v string) bool {
	for _, x := range akm.AuthKeys {
		if x == v {
			return true
		}
	}
	return false
}

func (akm *AppKeyMiddleware) Handle(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		if !akm.Filter(c) {

			c.Set("isAllowedApp", false)
			key := c.QueryParam("key")

			// has key
			if akm.keyExist(key) {
				c.Set("isAllowedApp", true)
				return next(c)
			}

		}

		return next(c)
	}
}
