package obsmiddleware

import (
	"crypto/rsa"
	"encoding/csv"
	"fmt"
	"strings"

	"bitbucket.org/observador/obs-utils/v2/model"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)

// Authenticator validate user auth token, see https://firebase.google.com/docs/auth/admin/verify-id-tokens
func JwtDecoder(verifyKey *rsa.PublicKey) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			var user model.User
			c.Set("user", &user)

			tokenSplit := strings.Split(c.Request().Header.Get("Authorization"), "Bearer ")

			tokenUrl := c.QueryParam("access_token")

			if len(tokenSplit) < 2 && tokenUrl == "" {
				return next(c)
			}

			tokenStr := tokenUrl

			if tokenStr == "" {
				tokenStr = tokenSplit[1]
			}

			tokenDecoded, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
				// Don't forget to validate the alg is what you expect:
				if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
					return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
				}
				return verifyKey, nil
			})
			if err != nil {
				return next(c)
			}

			claims, ok := tokenDecoded.Claims.(jwt.MapClaims)
			if !ok && !tokenDecoded.Valid {
				return next(c)
			}

			user.Verified = true
			user.Uid, _ = claims["sub"].(string)
			user.Name, _ = claims["name"].(string)
			user.Staff, _ = claims["staff"].(bool)

			risk, _ := claims["risk_level"].(float64)
			user.Risk = int(risk)

			user.Email, _ = claims["email"].(string)
			user.FamilyName, _ = claims["family_name"].(string)
			user.GivenName, _ = claims["given_name"].(string)
			user.Name, _ = claims["name"].(string)
			user.Nickname, _ = claims["nickname"].(string)
			user.Picture, _ = claims["picture"].(string)
			user.Staff, _ = claims["staff"].(bool)
			tags, _ := claims["tags"].(string)
			user.IsPremium = claims["is_premium"].(bool)

			t := csv.NewReader(strings.NewReader(tags))
			rtags, _ := t.Read()
			for _, value := range rtags {
				user.Tags = append(user.Tags, model.Tag{Name: value})
			}

			model.ParseTags(&user)

			return next(c)
		}
	}
}
