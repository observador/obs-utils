package obsmiddleware

import (
	"sync"
	"time"
	"github.com/labstack/echo"
	"os"

	"strconv"
)

type StatusMiddleware struct {
	lock              sync.RWMutex
	start             time.Time
	pid               int
	responseCounts    map[string]int
	totalResponseTime time.Duration
}

func (mw *StatusMiddleware) MiddlewareFunc() echo.MiddlewareFunc {
	mw.start = time.Now()
	mw.pid = os.Getpid()
	mw.responseCounts = map[string]int{}

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			start := time.Now()
			if err := next(c); err != nil {
				c.Error(err)
			}
			stop := time.Now()
			latency := stop.Sub(start)

			// count status
			status := strconv.Itoa(c.Response().Status)
			mw.responseCounts[status]++

			// times
			mw.totalResponseTime += latency

			return nil
		}
	}
}

type Status struct {
	Pid                    int            `json:"pid"`
	UpTime                 string         `json:"upTime"`
	UpTimeSec              float64        `json:"upTimeSec"`
	Time                   string         `json:"time"`
	TimeUnix               int64          `json:"timeUnix"`
	StatusCodeCount        map[string]int `json:"statusCodeCount"`
	TotalCount             int            `json:"totalCount"`
	TotalResponseTime      string         `json:"totalResponseTime"`
	TotalResponseTimeSec   float64        `json:"totalResponseTimeSec"`
	AverageResponseTime    string         `json:"averageResponseTime"`
	AverageResponseTimeSec float64        `json:"averageResponseTimeSec"`
}

func (mw * StatusMiddleware) GetAge() *time.Duration {
	upTime := time.Now().Sub(mw.start)
	return &upTime
}

func (mw *StatusMiddleware) GetStatus() *Status {
	mw.lock.RLock()
	now := time.Now()
	uptime := now.Sub(mw.start)
	totalCount := 0
	for _, count := range mw.responseCounts {
		totalCount += count
	}

	averageResponseTime := time.Duration(0)
	if totalCount > 0 {
		avgNs := int64(mw.totalResponseTime) / int64(totalCount)
		averageResponseTime = time.Duration(avgNs)
	}

	status := &Status{
		Pid:                    mw.pid,
		UpTime:                 uptime.String(),
		UpTimeSec:              uptime.Seconds(),
		Time:                   now.Format(time.RFC3339),
		TimeUnix:               now.Unix(),
		StatusCodeCount:        mw.responseCounts,
		TotalCount:             totalCount,
		TotalResponseTime:      mw.totalResponseTime.String(),
		TotalResponseTimeSec:   mw.totalResponseTime.Seconds(),
		AverageResponseTime:    averageResponseTime.String(),
		AverageResponseTimeSec: averageResponseTime.Seconds(),
	}

	mw.lock.RUnlock()

	return status
}
