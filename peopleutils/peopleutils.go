package peopleutils

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"
)

type PeopleApiConf struct {
	Url        string
	AuthKey    string
	AccessKeys []string
}

type Tag struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type Meta struct {
	ID     int64  `json:"-"`
	Name   string `json:"name"`
	Value  string `json:"value"`
	UserId int64  `json:"user_id"`
}

type User struct {
	ID          int64    `json:"-"`
	UserID      string   `json:"id"`
	DisplayName string   `json:"name"`
	GivenName   string   `json:"given_name"`
	FamilyName  string   `json:"family_name"`
	Email       string   `json:"email"`
	NIF         string   `json:"nif,omitempty"`
	Address     *Address `json:"address,omitempty"`
	Picture     string   `json:"picture"`
	RiskLevel   int64    `json:"risk"`
	Staff       bool     `json:"staff"`
	Commenter   bool     `json:"commenter"`
	Tags        []*Tag   `json:"tags"`
	Metadata    []*Meta  `json:"metadata"`
	Group       *Group   `json:"group,omitempty"`
}

type PaymentData struct {
	Name    string   `json:"name"`
	NIF     string   `json:"nif"`
	Email   string   `json:"email"`
	Address *Address `json:"address"`
}

type Address struct {
	Id         int64  `json:"id,omitempty"`
	Address    string `json:"address,omitempty"`
	City       string `json:"city,omitempty"`
	PostalCode string `json:"postal_code,omitempty"`
	Country    string `json:"country,omitempty"`
}

type Group struct {
	Id         string    `json:"id,omitempty"`
	Name       string    `json:"name,omitempty"`
	NIF        string    `json:"nif,omitempty"`
	AdminId    string    `json:"admin_id,omitempty"`
	AdminName  string    `json:"admin_name,omitempty"`
	AdminEmail string    `json:"admin_email,omitempty"`
	Address    *Address  `json:"address,omitempty"`
	State      string    `json:"state,omitempty"`
	CreatedAt  time.Time `json:"created_at,omitempty"`
}

func (pconf *PeopleApiConf) GetPaymentData(id string) (paymentData *PaymentData, err error) {

	req, err := http.NewRequest("GET", pconf.Url+"/paymentData/"+id+"?key="+pconf.AuthKey, nil)
	if err != nil {
		return
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-Adamastor-API-Version", "1.0.0")
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("error in request")
		return
	}
	defer res.Body.Close()
	if res.StatusCode >= 300 {
		log.Printf("error in response. code: %v", res.StatusCode)
		err = errors.New(fmt.Sprintf("error in response. code: %v", res.StatusCode))
		return
	}

	decoder := json.NewDecoder(res.Body)
	err = decoder.Decode(&paymentData)
	if err != nil {
		log.Println("error in decode")
		return
	}

	return
}

func (pconf *PeopleApiConf) GetUser(userID string) (user *User, err error) {
	if userID == "" {
		err = errors.New("invalid userId argument")
		return
	}

	req, err := http.NewRequest("GET", pconf.Url+"/users/"+userID+"?key="+pconf.AuthKey, nil)
	if err != nil {
		return
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-Adamastor-API-Version", "1.0.0")
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("error in request")
		return
	}
	defer res.Body.Close()
	if res.StatusCode >= 300 {
		log.Printf("error in response. code: %v", res.StatusCode)
		err = errors.New(fmt.Sprintf("error in response. code: %v", res.StatusCode))
		return
	}

	decoder := json.NewDecoder(res.Body)
	err = decoder.Decode(&user)
	if err != nil {
		log.Println("error in decode", err)
		return
	}

	return
}

func (pconf *PeopleApiConf) CanComment(userID string) bool {

	req, err := http.NewRequest("GET", pconf.Url+"/users/"+userID+"/canComment?key="+pconf.AuthKey, nil)
	if err != nil {
		log.Println("error creating request")
		return false
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-Adamastor-API-Version", "1.0.0")
	req.Header.Add("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("error in request")
		return false
	}
	defer res.Body.Close()
	if res.StatusCode >= 300 {
		log.Println("error in response. code:", res.StatusCode)
		err = errors.New(fmt.Sprintf("error in response. code: %v", res.StatusCode))
		return false
	}

	var canComment interface{}
	decoder := json.NewDecoder(res.Body)
	err = decoder.Decode(&canComment)
	if err != nil {
		log.Println("error in decode")
		return false
	}
	return canComment.(map[string]interface{})["canComment"].(bool)
}

func (pconf *PeopleApiConf) IsStaff(userID, key string) bool {
	for _, k := range pconf.AccessKeys {
		if k == key {
			return true
		}
	}

	user, err := pconf.GetUser(userID)

	if err != nil {
		log.Println("error in decode", err)
		return false
	}
	log.Println(user.Tags)
	for _, tag := range user.Tags {
		return tag.Name == "staff" || tag.Name == "admin"
	}
	return false
}

func (pconf *PeopleApiConf) SetUserGroupState(groupId string, state string) (err error) {

	req, err := http.NewRequest("PUT", pconf.Url+"/group/"+groupId+"/state/"+state+"?key="+pconf.AuthKey, nil)
	if err != nil {
		return
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-Adamastor-API-Version", "1.0.0")
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("error in request")
		return
	}
	defer res.Body.Close()

	if res.StatusCode >= 300 {
		log.Println("error in response. code:", res.StatusCode)
		err = errors.New(fmt.Sprintf("error in response. code: %v", res.StatusCode))
		return
	}

	return
}
func (pconf *PeopleApiConf) RemoveUserFromGroup(groupId string, email string) (err error) {

	req, err := http.NewRequest("DELETE", pconf.Url+"/group/"+groupId+"/user/"+email+"?key="+pconf.AuthKey, nil)
	if err != nil {
		return
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-Adamastor-API-Version", "1.0.0")
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("error in request")
		return
	}
	defer res.Body.Close()

	if res.StatusCode >= 300 {
		log.Println("error in response. code:", res.StatusCode)
		err = errors.New(fmt.Sprintf("error in response. code: %v", res.StatusCode))
		return
	}

	return
}
func (pconf *PeopleApiConf) DeleteGroup(groupId string) (err error) {

	req, err := http.NewRequest("DELETE", pconf.Url+"/group/"+groupId+"?key="+pconf.AuthKey, nil)
	if err != nil {
		return
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-Adamastor-API-Version", "1.0.0")
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("error in request")
		return
	}
	defer res.Body.Close()

	if res.StatusCode >= 300 {
		log.Println("error in response. code:", res.StatusCode)
		err = errors.New(fmt.Sprintf("error in response. code: %v", res.StatusCode))
		return
	}

	return
}

func (pconf *PeopleApiConf) UpdateUser(user User) (err error) {
	jsonUser, err := json.Marshal(user)
	if err != nil {
		log.Println("error on marshal of user struct")
		return
	}

	req, err := http.NewRequest("PUT", pconf.Url+"/api/users/"+user.UserID+"?key="+pconf.AuthKey, bytes.NewBuffer(jsonUser))
	if err != nil {
		return
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-Adamastor-API-Version", "1.0.0")
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)

	if err != nil {
		log.Println("error in request")
		return
	}
	defer res.Body.Close()

	if res.StatusCode >= 300 {
		log.Println("error in response. code:", res.StatusCode)
		err = errors.New(fmt.Sprintf("error in response. code: %v", res.StatusCode))
		return
	}

	return
}
