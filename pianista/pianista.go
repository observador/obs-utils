package pianista

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
)

type PianistApiConf struct {
	Url     string
	AuthKey string
}

type GroupResource struct {
	HasGroupResource bool  `json:"has_group_resource"`
	Slot             int64 `json:"slots"`
}

func (pAConf *PianistApiConf) HasGroupResource(userId string) *GroupResource {
	data := &GroupResource{}

	req, err := http.NewRequest("GET", pAConf.Url+"/users/"+userId+"/hasGroupResource?key="+pAConf.AuthKey, nil)
	if err != nil {
		return data
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("error in request")
		return data
	}
	if res.StatusCode >= 300 {
		log.Println("error in response. code: %v", res.StatusCode)
		err = errors.New(fmt.Sprintf("error in response. code: %v", res.StatusCode))
		return data
	}

	decoder := json.NewDecoder(res.Body)
	err = decoder.Decode(&data)
	res.Body.Close()
	if err != nil {
		log.Println("error in decode")
		return data
	}

	return data
}
